function LoadCharts() {
	Chart.defaults.global.responsive = true;
	Chart.defaults.Line.bezierCurve = true; 
	Chart.defaults.Line.pointDotStrokeWidth = 4;
	Chart.defaults.Line.pointDotRadius = 8;
	Chart.defaults.Line.datasetStrokeWidth = 4;
	LoadTrendChart();
	LoadBarChart(); 
}

function LoadGradeAverage() {
	
	Chart.defaults.global.responsive = true;
	Chart.defaults.Line.bezierCurve = false; 
	Chart.defaults.Line.pointDotStrokeWidth = 4;
	Chart.defaults.Line.pointDotRadius = 8;
	Chart.defaults.Line.datasetStrokeWidth = 4;
	var verlauf = document.getElementById("verlauf").getContext("2d");
	var data = {
		labels: ["1.Sem", "2.Sem", "3.Sem", "4.Sem", "5.Sem", "6.Sem", "7.Sem"],
		datasets: [
			{
				label: "My First dataset",
				fillColor: "rgba(220,220,220,0)",
				strokeColor: "rgba(40,99,175,1)",
				pointColor: "rgba(255,255,255,1)",
				pointStrokeColor: "rgba(40,99,175,1)",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: [2.0, 2.2, 2.4, 2.8, 2.4, 2.2,1.9 ]
			}
		]
	};
					
				
	new Chart(verlauf).Line(data);	
	
}


function LoadTrendChart(){
			
	var trend = document.getElementById("trend").getContext("2d");
	var data2 = {
					labels: ["2.Sem", "3.Sem", "4.Sem"],
					datasets: [
									{
										label: "My First dataset",
										fillColor: "rgba(220,220,220,0.2)",
										strokeColor: "rgba(220,220,220,1)",
										pointColor: "rgba(220,220,220,1)",
										pointStrokeColor: "#fff",
										pointHighlightFill: "#fff",
										pointHighlightStroke: "rgba(220,220,220,1)",
										data: [2.2, 2.2, 2.2]
									},
									{
										label: "My Second dataset",
										fillColor: "rgba(255, 200, 200,0.2)",
										strokeColor: "rgba(255, 200, 200,1)",
										pointColor: "rgba(255, 200, 200,1)",
										pointStrokeColor: "#fff",
										pointHighlightFill: "#fff",
										pointHighlightStroke: "rgba(151,187,205,1)",
										data: [2.1, 2.4, 2.8]
									}
								]
							}
							
				new Chart(trend).Line(data2);

}

function LoadBarChart() {
	
	var graph = document.getElementById("verteilung").getContext("2d");
	var data = {
					labels: ["1.0", "1.3", "1.7", "2.0", "2.3", "2.7", "3.0", "3.3", "3.7", "4.0",],
					datasets: [
							{
								label: "My First dataset",
								fillColor: "rgba(220,220,220,0.75)",
								strokeColor: "rgba(220,220,220,1)",
								highlightFill: "rgba(40,99,175,0.9)",
								highlightStroke: "rgba(220,220,220,1)",
								data: [2, 1, 3, 5, 1, 1, 2, 4, 2, 3]
							}
						]
					};
					
	new Chart(graph).Bar(data);
	
}

                    
					
					
					
					
					
					