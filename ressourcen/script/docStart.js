function loadHeader() {

	$("body").prepend('<div id="header"></div>');
	$("#header").append('<header id="startHeader"></header>');
	$("#startHeader").append('<h1 id="username">Lisa <strong>Müller</strong> | 4.Semester</h1>');
	$("#startHeader").append('<h2 id="studiengang">Informatik - Bachelor of Science</h2>');


}

function loadNavigation() {
	
	$("body").prepend('<div id="navigation"></div>');
	$("#navigation").append('<nav id="startNavbar" class="navbar navbar-default" style="margin:auto;border-radius: 0px;"></nav>');
	$("#startNavbar").append('<div id="innerHeader" class="navbar-header"></div>');
	$("#startNavbar").append('<div class="collapse navbar-collapse" id="Navbar" ></div>');
	
	$("#innerHeader").append('<button id="innerHeaderToogle" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Navbar"></button>');
	$("#innerHeaderToogle").append('<span class="icon-bar"></span>');
	$("#innerHeaderToogle").append('<span class="icon-bar"></span>');
	$("#innerHeaderToogle").append('<span class="icon-bar"></span>');
	
	$("#Navbar").append('<ul id="navMenu" class="nav navbar-nav"></ul>');
	$("#navMenu").append('<li><a id="nav_home"><span class="glyphicon glyphicon-home"></a></li>');
	$("#navMenu").append('<li><a id="nav_studium"><b>Studium</b></a></li>');
	$("#navMenu").append('<li><a id="nav_termine"><b>Termine</b></a></li>');
	$("#navMenu").append('<li><a id="nav_todos"><b>ToDos</b></a></li>');
	$("#navMenu").append('<li><a id="nav_plan"><b>Lernplan</b></a></li>');
	
	$("#Navbar").append('<ul id="navbarRight" class="nav navbar-nav navbar-right"></ul>');
	$("#navbarRight").append('<li><a id="nav_settings"><span class="glyphicon glyphicon-cog"></span><strong>  Einstellungen</strong></a></li>');
	
}