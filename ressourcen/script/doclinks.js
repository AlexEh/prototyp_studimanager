/*Dieses Skript verwaltet alle Links im Pototypen an einer zentralen Stelle. */

function SetLinksOnHeader() {
					$("#nav_home").attr("href", "index.html");
					$("#nav_studium").attr("href", "studium_notenaktuell.html");
					$("#nav_dok").attr("href", "studium_coming_soon.html");
					$("#nav_termine").attr("href", "studium_coming_soon.html");
					$("#nav_todos").attr("href", "studium_coming_soon.html");
					$("#nav_plan").attr("href", "studium_coming_soon.html");
					
					$("#navNoten").attr("href", "studium_notenaktuell.html");
					$("#navAktuelleNoten").attr("href", "studium_notenaktuell.html");
					$("#navNotenverlauf").attr("href", "studium_notenverlauf.html");
					$("#navNotenszenarien").attr("href", "studium_notenszenarien.html");
					$("#navStudiumverlauf").attr("href", "studium_studienreport.html");
					$("#navStudienplanung").attr("href", "studium_studiumplanung.html");
					$("#navStudienreport").attr("href", "studium_studienreport.html");
					$("#navPrüfungen").attr("href", "studium_pruefungen.html");
}